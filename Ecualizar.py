# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Descripción: Graficacion de una imagen, antes y despues de ecualizar

import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('../Externo/dibujo.jpg')
b,g,r = cv.split(img)       # get b,g,r
rgb_img = cv.merge([r,g,b])

img_to_yuv = cv.cvtColor(img, cv.COLOR_BGR2YUV)
img_to_yuv[:, :, 0] = cv.equalizeHist(img_to_yuv[:, :, 0])
hist_equalization_result = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)

cv.imwrite('../Externo/resultimagen.jpg', hist_equalization_result)
resul=cv.imread('../Externo/resultimagen.jpg')
b,g,r = cv.split(resul)
rgb_resul = cv.merge([r,g,b])

# Posicion y muestra de cada imagen y grafico en la pantalla
plt.subplot(221), plt.imshow(rgb_img, 'gray')
plt.subplot(223), plt.imshow(rgb_resul,'gray')

color = ('b','g','r')
for i,col in enumerate(color):
    histr = cv.calcHist([img],[i],None,[256],[0,256])
    plt.subplot(222),plt.plot(histr,color = col)
    histrecualizada = cv.calcHist([resul], [i], None, [256], [0, 256])
    plt.subplot(224), plt.plot(histrecualizada, color=col)

plt.xlim([0,256])
plt.show()