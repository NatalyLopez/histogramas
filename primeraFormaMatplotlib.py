# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Descripción: Graficacion de histogramas de una imagen en escala de grises aplicando una mascara

import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('../Externo/dibujo.jpg',0)
# Crea una mascara
mask = np.zeros(img.shape[:2], np.uint8)
mask[100:300, 100:400] = 255
masked_img = cv.bitwise_and(img,img,mask = mask)
# Calcular histograma con máscara y sin máscara
# Verifica el tercer argumento para la máscara
hist_full = cv.calcHist([img],[0],None,[256],[0,256])
hist_mask = cv.calcHist([img],[0],mask,[256],[0,256])
# Posicion y muestra de cada imagen y grafico en la pantalla
plt.subplot(221), plt.imshow(img, 'gray')
plt.subplot(222), plt.imshow(mask,'gray')
plt.subplot(223), plt.imshow(masked_img, 'gray')
plt.subplot(224), plt.plot(hist_full), plt.plot(hist_mask)
plt.xlim([0,256])
plt.show()