# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Descripción: Graficacion de histogramas de una imagen en escala BGR

import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('../Externo/dibujo.jpg')
color = ('b','g','r')
# Para cada color B, G y R encuentra histograma y lo traza
for i,col in enumerate(color):
    histr = cv.calcHist([img],[i],None,[256],[0,256])
    plt.plot(histr,color = col)
    plt.xlim([0,256])
# Muestra pantalla de imagen con titulo y pantalla de histograma
cv.imshow('Dibujo',img); plt.show()
# Espera para que la ventana se destruya
cv.waitKey(0)
# Cierra la ventana
cv.destroyAllWindows()