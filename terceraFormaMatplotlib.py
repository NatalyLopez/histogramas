# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Descripción: Graficacion de histogramas de una imagen en escala de grises

import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('../Externo/dibujo.jpg',0)
# Encuentra directamente el histograma y lo traza
plt.hist(img.ravel(),256,[0,256])
# Muestra pantalla de imagen con titulo y pantalla de histograma
cv.imshow('Dibujo',img); plt.show()
# Espera para que la ventana se destruya
cv.waitKey(0)
# Cierra la ventana
cv.destroyAllWindows()